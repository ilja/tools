<!--
SPDX-FileCopyrightText: 2024 ilja@space

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# Tools

This repository contains custom tools in the form of web applications. They can be freely used thanks to [Codeberg pages](https://docs.codeberg.org/codeberg-pages/).

Check it out at <https://ilja.codeberg.page/tools/>.

## Scope

The idea is to have separate simple webapp tools. A "tool" should be smoll and self contained in a single file.

You can link from the index file and back, but that's about it. There's no "shared code" between tools. Snippets can demo small things who can be copy-pasted, but there shouldn't be any shared code that different tools call during runtime.

## Reading

The index.html is a page with hard-coded links to the different tools. Each of the other html pages is it's own tool with self-contained code.

The snippets folder contains html pages to demo something which tools can use. E.g. how a footer can be properly implemented, etc.

## License

    Custom tools in the form of web applications
    Copyright (C) 2024  ilja@ilja.space

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
